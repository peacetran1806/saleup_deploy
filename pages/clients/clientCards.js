const CLientCards = () => {
    return (
      <div className="row">
        <div className="col-3 client-col">
          <div className="client-card">
            <a href="#" className="card-link">
              <span className="card-img"></span>
              <span className="card-body">
                <span className="h3 heading">20</span>
                <p>Visited Customers Today</p>
              </span>
            </a>
          </div>
        </div>
        <div className="col-3 client-col">
          <div className="client-card">
            <a href="#" className="card-link">
              <span className="card-img"></span>
              <span className="card-body">
                <span className="h3 heading">20</span>
                <p>New Cusomer This Month</p>
              </span>
            </a>
          </div>
        </div>
        <div className="col-3 client-col">
          <div className="client-card">
            <a href="#" className="card-link">
              <span className="card-img"></span>
              <span className="card-body">
                <span className="h3 heading">15</span>
                <p>Total Customers</p>
              </span>
            </a>
          </div>
        </div>
        <div className="col-3 client-col">
          <div className="client-card">
            <a href="#" className="card-link">
              <span className="card-img"></span>
              <span className="card-body">
                <span className="h3 heading">10</span>
                <p>Total Free Reward Customers</p>
              </span>
            </a>
          </div>
        </div>
        <div className="col-3 client-col">
          <div className="client-card">
            <a href="#" className="card-link">
              <span className="card-img"></span>
              <span className="card-body">
                <span className="h3 heading">50</span>
                <p>Visited Customers This Weeks</p>
              </span>
            </a>
          </div>
        </div>
        <div className="col-3 client-col">
          <div className="client-card">
            <a href="#" className="card-link">
              <span className="card-img"></span>
              <span className="card-body">
                <span className="h3 heading">21</span>
                <p>Visited Customers Today</p>
              </span>
            </a>
          </div>
        </div>
        <div className="col-3 client-col">
          <div className="client-card">
            <a href="#" className="card-link">
              <span className="card-img"></span>
              <span className="card-body">
                <span className="h3 heading">15</span>
                <p>
                  Visited Free Reward
                  <br />
                  Customers This Week
                </p>
              </span>
            </a>
          </div>
        </div>
        <div className="col-3 client-col">
          <div className="client-card">
            <a href="#" className="card-link">
              <span className="card-img"></span>
              <span className="card-body">
                <span className="h3 heading">25</span>
                <p>
                  New Free Reward
                  <br />
                  Customers This Week
                </p>
              </span>
            </a>
          </div>
        </div>
      </div>
    )
  }
  
  export default CLientCards
  