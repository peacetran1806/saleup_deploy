const MyAccount_Account = () => {
    return (
        <div className="row">
            <div className="col-md-5">
                <h4>My Account</h4>
            </div>
            <div className="col-md-6">
                <div className="box box-danger">
                    <form role="form">
                        <div className="box-body">
                            <div className="form-inline">
                                <div className="form-group mb-2">
                                    <div style={{ borderRadius: '50%', width: '50px', height: '50px', border: '1px solid black' }}></div>
                                    <button type="button" className="btn btn-default">Upload Image</button>
                                    <button type="button" className="btn btn-default">Remove Image</button>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-6">
                                    <div className="form-group">
                                        <label>First Name</label>
                                        <input type="text" className="form-control" placeholder="First Name" />
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <div className="form-group">
                                        <label>Last Name</label>
                                        <input type="text" className="form-control" placeholder="Last Name" />
                                    </div>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-lg-6">
                                    <div className="form-group">
                                        <label>Email</label>
                                        <input type="email" className="form-control" placeholder="" />
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <div className="form-group">
                                        <label>Phone Number</label>
                                        <input type="text" className="form-control" placeholder="" />
                                    </div>
                                </div>
                            </div>
                            <hr></hr>
                            <div className="row">
                                <div className="col-lg-12">
                                    <label>Notification</label><br></br>
                                    <input type="checkbox"/> <label>Allow the important notification to your email</label><br></br>
                                    <label>lkjasdklfjklasdj asldjfklasjd klajsdfklja sdlkjfalksd</label>
                                </div>
                            </div>
                            <hr></hr>
                            <div className="row">
                                <div className="col-lg-12">
                                    <label>CHANGE YOUR PASSWORD</label><br></br>
                                    <label>Change your password to login to SaleUp again.</label>
                                </div>

                            </div>
                            <div className="row">
                                <div className="col-lg-6">
                                    <div className="form-group">
                                        <label>Email</label>
                                        <input type="email" className="form-control" placeholder="" />
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <div className="form-group">
                                        <label>Phone Number</label>
                                        <input type="text" className="form-control" placeholder="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    )
}

export default MyAccount_Account 