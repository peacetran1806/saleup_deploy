
import SideBar from './SideBar'
import NavBar from './NavBar'
const SiteLayout = ({ children }) => (
    <>
        <header className="main-header">
            <a href="index2.html" className="logo" style={{backgroundColor:'white'}}>
                <span className="logo-mini" style={{ display: 'none' }}>SALEUP</span>
                <span className="logo-lg"><img className="img-fluid mt-2" src="/img/logo.png"></img></span>
                {/* <span className="material-icons">menu_open</span> */}
            </a>
            <NavBar />

            {children}
        </header>
        <SideBar />
    </>

);

export const getLayout = page => <SiteLayout>{page}</SiteLayout>;

export default SiteLayout;
