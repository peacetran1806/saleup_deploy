import authHeader from '../../services/auth-header';

const getAllStore = () =>{
  return fetch('https://api.saleup.com.au/graphql', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': authHeader().Authorization
    },
    body: JSON.stringify({
      query: `query stores{
                    stores{
                      name
                      owner{
                        username
                        email
                        firstName
                        lastName
                        phone
                          }
                      address
                      phone
                      website
                      businessHours{
                          weekDay
                              isWorking
                          openHour
                          closeHour
                      }
                    }
                  }`}),
  })
    .then((r) => {
      return r.json();
    })
    .then((response) => {
      return response;
    });

}

const createStore = (query, variables) => {
  return fetch('https://api.saleup.com.au/graphql', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      query: query,
      variables: variables
    })
  })
    .then((r) => {
      return r.json()
    })
    .then((data) => {
      return data
    });
}


export const StoreService = {
  getAllStore,
  createStore
};